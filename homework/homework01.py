# Задание 1. Встроенные типы данных, операторы, функции и генераторы
#
# Напишите реализации объявленных ниже функций. Для проверки
# корректности реализации ваших функций, запустите тесты:
#
# pytest test_homework01.py
#
# Если написанный вами код не содержит синтаксических ошибок,
# вы увидите результаты тестов ваших решений.

from timeit import timeit
from functools import reduce
from operator import mul
from itertools import islice

def fac(n):
    """
    Факториал

    Факториал числа N - произведение всех целых чисел от 1 до N
    включительно. Например, факториал числа 5 - произведение
    чисел 1, 2, 3, 4, 5.

    Функция должна вернуть факториал аргумента, числа n.
    """
    if n == 0:
        return 1
    else:
        a = 1
        for i in range(2, n + 1):
            a *= i
        return a

n = 10

# print(timeit(lambda: fac(n)))
# print(fac(n))

def fac2(n):
    if n==0:
        return 1
    else:
        return n * fac2(n-1)
# print(timeit(lambda: fac2(n)))
# print(fac2(n))

def fac3(n):
    if n==0:
        return 1
    for x in range(1,n):
        n *= x
    return n
# print(timeit(lambda: fac3(n)))
# print(fac3(n))

def fac4(n):
    return reduce(mul, range(1, n+1))
# print(timeit(lambda: fac4(n)))
# print(fac4(n))


def gcd(a, b):
    """
    Наибольший общий делитель (НОД) для двух целых чисел.

    Предполагаем, что оба аргумента - положительные числа
    Один из самых простых способов вычесления НОД - метод Эвклида,
    согласно которому

    1. НОД(a, 0) = a
    2. НОД(a, b) = НОД(b, a mod b)

    (mod - операция взятия остатка от деления, в python - оператор '%')
    """
    if a == b:
        #print ("r1")
        return a
    elif a == 0:
        #print ("r2")
        return b
    elif b == 0:
        #print ("r3")
        return a
    else:
        if a > b:
            numMax = a
            numMin = b
        else:
            numMax = b
            numMin = a
        while numMax % numMin != 0:
            numMax, numMin = numMin, numMax % numMin
        #print("r4")
        return numMin


def fib():
    """
    Генератор для ряда Фибоначчи

    Вам необходимо сгенерировать бесконечный ряд чисел Фибоначчи,
    в котором каждый последующий элемент ряда является суммой двух
    предыдущих. Начало последовательности: 1, 1, 2, 3, 5, 8, 13, ..

    Подсказка по реализации: для бесконечного цикла используйте идиому

    while True:
      ..

    """

    num1 = 1
    num2 = 1
    #print(num1)
    yield num1
    while True:
        #print(num2)
        yield num2
        total = num1 + num2
        num1, num2 = num2, total


def fib2():
    a = b = 1
    while True:
        yield a
        a, b = b,  a + b


def fib3(n):
    if n < 2:
        return 1
    else:
        return fib3(n - 1) +  fib3(n - 1)

# print(list(islice(f, 10)))

# n = 0
# for i in fib():
#     if i < 10000000000000000000000:
#         print(i)
#         n = i
#     else:
#         break 



def flatten(seq):
    """
    Функция, преобразующая вложенные последовательности любого уровня
    вложенности в плоские, одноуровневые.

    >>> flatten([])
    []
    >>> flatten([1, 2])
    [1, 2]
    >>> flatten([1, [2, [3]]])
    [1, 2, 3]
    >>> flatten([(1, 2), (3, 4)])
    [1, 2, 3, 4]
    """
    def generatorFlatten(seq):
        for i in seq:
            if isinstance(i, (list, tuple)):
                for x in generatorFlatten(i):
                    yield x
            else:
                yield i
    return list(generatorFlatten(seq))

lst = [100, "sdsdd", (1, [1,2,[3]]), (3, 4)]
print(timeit(lambda: flatten(lst)))
print(list(flatten(lst)))


def flatten2(seq):
    acc = []
    for e in seq:
        # isinstance(e, Sequence)  # from collections.abc import Sequence
        # hasattr(e, "__iter__")
        if type(e) in {list, tuple}:
            acc.extend(flatten(e)) # розширення списку іншим списком
        else:
            acc.append(e) # додає один елмент до спииску
    return acc

print(timeit(lambda: flatten2(lst)))
print(list(flatten2(lst)))


def flatten3(seq):
    for e in seq:
        if type(e) in {list, tuple}:
            yield from flatten3(e)  # тільки Пайтон 3
        else:
            yield e

print(timeit(lambda: flatten3(lst)))
print(list(flatten3(lst)))